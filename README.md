# Arducam

Desarrollos realizados para trabajar con la plataforma Arducam y las imagenes capturadas a partir de la misma.

El repositorio cuenta con varias carpetas, las cuales tienen el siguiente contenido:
- Capturador: Nuevo script de captura y analisis de datos, es la version mas actual utilizada.
- Analizador: listo
- Resultados: Contiene al análisis de una linea y resolucion
- Resultado: Contiene los scripts actualizados del analizador