"""
Definir.py

El script permite definir regiones rectangulares donde se realizar posteriores análisis,
almacenando un archivo que contiene sus definiciones.

Utilización:
-f, --fondo:       Carpeta de fondo
-c, --captura:     Carpeta de capturas

Requerimientos:
Para correr este script se requiere que los siguientes módulos estén instalados:
- os
- numpy
- astropy.io
- argparse
- sys
- matplotlib

@Author: Leo Corzi
@Email: damian.corzi@ib.edu.ar
@Date: Thu May 26 2022, 12:48:41
"""

from ast import Global
from cProfile import label
import os
import numpy as np
from astropy.io import fits
import argparse
from scipy.stats import norm
import matplotlib.patches as patches
import matplotlib.pyplot as plt
import pickle

# In[0]: Parser 
parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)

#Lista de argumentos esperados, tipo y valores por defecto
parser.add_argument('-f', '--fondo',   type=str, action="store",      dest='f', default="fondo_con_radiacion_30kV_40uA_exp-16383.gan-32.off-2",   help='Carpeta de fondo')
parser.add_argument('-c', '--captura', type=str, action="store",      dest='c', default="prueba4_escalera_30kV_40uA_exp-16383.gan-32.off-2", help='Carpeta de captura')

#Variable que almacena los resultados
args = parser.parse_args()

# In[1] : Funciones
def Separar(img):
    rows, cols = np.indices(img.shape)
    imgA = img[(rows%2)==(cols%2)]
    imgB = img[(rows%2)!=(cols%2)]

    return imgA, imgB

def Generar_Texto(zer,sat,medA,medB,stdA,stdB,med,dif):
    texto = "| Resultados ||||\n|-|-|-|-|\n"\
            f"| Media A | {medA:.2f}| Media B | {medB:.2f}|\n"\
            f"| Std A | {stdA:.2f}| Std B | {stdB:.2f}|\n"\
            f"| Media | {med:.2f}| Diferencia | {dif:.2f}|\n"\
            f"| Pixeles en 0 | {zer:.2f}%| Pixeles en 255 | {sat:.2f}%|\n\n "
    return texto

def Escribir_MD(texto, path = "Readme.md"):
    
    with open(path, "a+") as file:
        file.seek(0)
        if len(file.read(1)) == 0:
            cabecera = "# Archivo de Resultados\n"\
                       "Analisis realizados utilizando el script {:}, cuyo `Help` el siguiente:\n\n```python\n{:}```\n\n".format(__file__, __doc__)
            file.write(cabecera+texto)

            os.system("chmod a+rw " + path)
        else:
            file.write(texto)

def Eliminar(path):
	if os.path.exists(path):
		os.system("rm " + path)

def Almacenar(NameFile,parametros):
	Eliminar(NameFile)
	archivo = open(NameFile, 'wb')
	pickle.dump(parametros, archivo)
	archivo.close()


# In[2]: Variables globales

#Editar esta linea segun cada caso
Espesores = [0.1,0.4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]

# In[3]: Procesamiento
def Analizar_CF(cname, sname, fname):
    with fits.open(cname + "/Captura.fits") as hduc, fits.open(sname + "/Captura.fits") as hdfs, fits.open(fname + "/Captura.fits") as hdff:

        #Defino imagen
        captura = hduc[0].data
        fondos  = hdfs[0].data
        flat    = hdff[0].data
        
        images  = (captura - fondos)/(flat - fondos)

        #Imagen de diferencias
        name=cname + "/Diferencia.fits"
        hdu=fits.PrimaryHDU()
        hdu.data=images
        hdu.writeto(name,overwrite=True)

        name = "Rectangulos.pickle"
        with open(name, 'rb') as file:
            areas = pickle.load(file)

            MED = []
            STD = []

            for area in areas:

                sdata = images[area[2]:area[3],area[0]:area[1]]
                med = sdata.reshape(-1).mean()
                std = sdata.reshape(-1).std()

                #Guardamos resultados
                MED.append(med)
                STD.append(std)
    
        return MED, STD

def Analizar_SF(cname, sname):
    with fits.open(cname + "/Captura.fits") as hduc, fits.open(sname + "/Captura.fits") as hdfs:

        #Defino imagen
        captura = hduc[0].data
        fondos  = hdfs[0].data
        images  = captura - fondos

        name = "Rectangulos.pickle"
        with open(name, 'rb') as file:
            areas = pickle.load(file)

            MED = []
            STD = []

            for area in areas:

                sdata = images[area[2]:area[3],area[0]:area[1]]
                med = sdata.reshape(-1).mean()
                std = sdata.reshape(-1).std()

                #Guardamos resultados
                MED.append(med)
                STD.append(std)
    
        return MED, STD

#Ganancias
G = [1,4,9,16,32]

#Almacenamiento
M0 = []
M1 = []
M2 = []
M3 = []

S0 = []
S1 = []
S2 = []
S3 = []

for g in G:
    fonds = f"fondo_sin_radiacion_exp-16383.gan-{g}.off-2"

    captu = f"prueba4_escalera_30kV_40uA_exp-16383.gan-{g}.off-2"

    MED, STD = Analizar_SF(captu, fonds)

    M0.append(MED)
    S0.append(STD)

    captu = f"prueba4_escalera_40kV_40uA_exp-16383.gan-{g}.off-2"

    MED, STD = Analizar_SF(captu, fonds)

    M1.append(MED)
    S1.append(STD)

    captu = f"prueba4_escalera_60kV_20uA_exp-16383.gan-{g}.off-2"

    MED, STD = Analizar_SF(captu, fonds)

    M2.append(MED)
    S2.append(STD)

M0 = np.array(M0).transpose()
M1 = np.array(M1).transpose()
M2 = np.array(M2).transpose()

S0 = np.array(S0).transpose()
S1 = np.array(S1).transpose()
S2 = np.array(S2).transpose()

fig = plt.figure(1)
plt.errorbar(G, M0[0], yerr = S0[0], label='30kv, 40mA, 1mm', marker='X', mfc='red', mec='red', linestyle= "-", color = "red")
plt.errorbar(G, M0[1], yerr = S0[1], label='30kv, 40mA, 4mm', marker='+', mfc='blue', mec='blue', linestyle= "-", color = "blue")
plt.errorbar(G, M0[2], yerr = S0[2], label='30kv, 40mA, 0mm', marker='*', mfc='green', mec='green', linestyle= "-", color = "green")

plt.errorbar(G, M1[0], yerr = S1[0], label='40kv, 40mA, 1mm', marker='s', mfc='red', mec='red', linestyle= "--", color = "red")
plt.errorbar(G, M1[1], yerr = S1[1], label='40kv, 40mA, 4mm', marker='p', mfc='blue', mec='blue', linestyle= "--", color = "blue")
plt.errorbar(G, M1[2], yerr = S1[2], label='40kv, 40mA, 0mm', marker='^', mfc='green', mec='green', linestyle= "--", color = "green")

plt.errorbar(G, M2[0], yerr = S1[0], label='50kv, 20mA, 1mm', marker='1', mfc='red', mec='red', linestyle= "-.", color = "red")
plt.errorbar(G, M2[1], yerr = S2[1], label='50kv, 20mA, 4mm', marker='2', mfc='blue', mec='blue', linestyle= "-.", color = "blue")
plt.errorbar(G, M2[2], yerr = S2[2], label='50kv, 20mA, 0mm', marker='3', mfc='green', mec='green', linestyle= "-.", color = "green")

plt.ylim(0,None)
plt.xlabel("Ganancia")
plt.ylabel("Intensidad de los Pixeles (ADU)")
plt.grid(True)
plt.legend()

fig = plt.figure(2)
plt.plot(G,M0[0]/M0[2],"rs-", label='30kv, 40mA, 1mm')
plt.plot(G,M0[1]/M0[2],"bo-", label='30kv, 40mA, 4mm')

plt.plot(G,M1[0]/M1[2],"r+--", label='40kv, 40mA, 1mm')
plt.plot(G,M1[1]/M1[2],"b^--", label='40kv, 40mA, 4mm')

plt.plot(G,M2[0]/M2[2],"rp-.", label='50kv, 20mA, 1mm')
plt.plot(G,M2[1]/M2[2],"bX-.", label='50kv, 20mA, 4mm')

plt.ylim(0,None)
plt.xlabel("Ganancia")
plt.ylabel("Transmisión")
plt.grid(True)
plt.legend()

#Almacenamiento
M0 = []
M1 = []
M2 = []
M3 = []

S0 = []
S1 = []
S2 = []
S3 = []

for g in G:
    fonds = f"fondo_sin_radiacion_exp-16383.gan-{g}.off-2"

    captu = f"prueba4_escalera_30kV_40uA_exp-16383.gan-{g}.off-2"
    flats = f"fondo_con_radiacion_30kV_40uA_exp-16383.gan-{g}.off-2"

    MED, STD = Analizar_CF(captu, fonds, flats)

    M0.append(MED)
    S0.append(STD)

    captu = f"prueba4_escalera_40kV_40uA_exp-16383.gan-{g}.off-2"
    flats = f"fondo_con_radiacion_40kV_40uA_exp-16383.gan-{g}.off-2"

    MED, STD = Analizar_CF(captu, fonds, flats)

    M1.append(MED)
    S1.append(STD)

    captu = f"prueba4_escalera_60kV_20uA_exp-16383.gan-{g}.off-2"
    flats = f"fondo_con_radiacion_60kV_20uA_exp-16383.gan-{g}.off-2"

    MED, STD = Analizar_CF(captu, fonds, flats)

    M2.append(MED)
    S2.append(STD)

M0 = np.array(M0).transpose()
M1 = np.array(M1).transpose()
M2 = np.array(M2).transpose()

S0 = np.array(S0).transpose()
S1 = np.array(S1).transpose()
S2 = np.array(S2).transpose()

fig = plt.figure(3)
plt.errorbar(G, M0[0], yerr = S0[0], label='30kv, 40mA, 1mm', marker='X', mfc='red', mec='red', linestyle= "-", color = "red")
plt.errorbar(G, M0[1], yerr = S0[1], label='30kv, 40mA, 4mm', marker='+', mfc='blue', mec='blue', linestyle= "-", color = "blue")
plt.errorbar(G, M0[2], yerr = S0[2], label='30kv, 40mA, 0mm', marker='*', mfc='green', mec='green', linestyle= "-", color = "green")

plt.errorbar(G, M1[0], yerr = S1[0], label='40kv, 40mA, 1mm', marker='s', mfc='red', mec='red', linestyle= "--", color = "red")
plt.errorbar(G, M1[1], yerr = S1[1], label='40kv, 40mA, 4mm', marker='p', mfc='blue', mec='blue', linestyle= "--", color = "blue")
plt.errorbar(G, M1[2], yerr = S1[2], label='40kv, 40mA, 0mm', marker='^', mfc='green', mec='green', linestyle= "--", color = "green")

plt.errorbar(G, M2[0], yerr = S1[0], label='50kv, 20mA, 1mm', marker='1', mfc='red', mec='red', linestyle= "-.", color = "red")
plt.errorbar(G, M2[1], yerr = S2[1], label='50kv, 20mA, 4mm', marker='2', mfc='blue', mec='blue', linestyle= "-.", color = "blue")
plt.errorbar(G, M2[2], yerr = S2[2], label='50kv, 20mA, 0mm', marker='3', mfc='green', mec='green', linestyle= "-.", color = "green")

plt.ylim(0,None)
plt.xlabel("Ganancia")
plt.ylabel("Intensidad Relativa")
plt.grid(True)
plt.legend()

fig = plt.figure(4)
plt.plot(G,M0[0]/M0[2],"rs-", label='30kv, 40mA, 1mm')
plt.plot(G,M0[1]/M0[2],"bo-", label='30kv, 40mA, 4mm')

plt.plot(G,M1[0]/M1[2],"r+--", label='40kv, 40mA, 1mm')
plt.plot(G,M1[1]/M1[2],"b^--", label='40kv, 40mA, 4mm')

plt.plot(G,M2[0]/M2[2],"rp-.", label='50kv, 20mA, 1mm')
plt.plot(G,M2[1]/M2[2],"bX-.", label='50kv, 20mA, 4mm')

plt.ylim(0,None)
plt.xlabel("Ganancia")
plt.ylabel("Transmisión")
plt.grid(True)
plt.legend()

fig = plt.figure(5)
plt.plot(G,-np.log(M0[0]/M0[2])/0.1,"rs-", label='30kv, 40mA, 1mm')
plt.plot(G,-np.log(M0[1]/M0[2])/0.4,"bo-", label='30kv, 40mA, 4mm')

plt.plot(G,-np.log(M1[0]/M1[2])/0.1,"r+--", label='40kv, 40mA, 1mm')
plt.plot(G,-np.log(M1[1]/M1[2])/0.4,"b^--", label='40kv, 40mA, 4mm')

plt.plot(G,-np.log(M2[0]/M2[2])/0.1,"rp-.", label='50kv, 20mA, 1mm')
plt.plot(G,-np.log(M2[1]/M2[2])/0.4,"bX-.", label='50kv, 20mA, 4mm')

plt.ylim(0,None)
plt.xlabel("Ganancia")
plt.ylabel("Coeficiente de Atenuación Asociado")
plt.grid(True)
plt.legend()


plt.show()
exit()