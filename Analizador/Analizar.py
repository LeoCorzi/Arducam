"""
Definir.py

El script permite definir regiones rectangulares donde se realizar posteriores análisis,
almacenando un archivo que contiene sus definiciones.

Utilización:
-f, --fondo:       Carpeta de fondo
-c, --captura:     Carpeta de capturas

Requerimientos:
Para correr este script se requiere que los siguientes módulos estén instalados:
- os
- numpy
- astropy.io
- argparse
- sys
- matplotlib

@Author: Leo Corzi
@Email: damian.corzi@ib.edu.ar
@Date: Thu May 26 2022, 12:48:41
"""

from ast import Global
from cProfile import label
import os
import numpy as np
from astropy.io import fits
import argparse
from scipy.stats import norm
import matplotlib.patches as patches
import matplotlib.pyplot as plt
import pickle

# In[0]: Parser 
parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)

#Lista de argumentos esperados, tipo y valores por defecto
parser.add_argument('-f', '--fondo',   type=str, action="store", dest='f', default="fondo_sin_radiacion_exp-16383.gan-32.off-2",   help='Carpeta de fondo')
parser.add_argument('-c', '--captura', type=str, action="store", dest='c', default="prueba4_escalera_30kV_40uA_exp-16383.gan-32.off-2",    help='Carpeta de captura')
parser.add_argument('-l', '--flat',    type=str, action="store", dest='l', default="fondo_con_radiacion_30kV_40uA_exp-16383.gan-32.off-2", help='Carpeta de flat')

#Variable que almacena los resultados
args = parser.parse_args()

# In[1] : Funciones
def Separar(img):
    rows, cols = np.indices(img.shape)
    imgA = img[(rows%2)==(cols%2)]
    imgB = img[(rows%2)!=(cols%2)]

    return imgA, imgB

def Generar_Texto(zer,sat,medA,medB,stdA,stdB,med,dif):
    texto = "| Resultados ||||\n|-|-|-|-|\n"\
            f"| Media A | {medA:.2f}| Media B | {medB:.2f}|\n"\
            f"| Std A | {stdA:.2f}| Std B | {stdB:.2f}|\n"\
            f"| Media | {med:.2f}| Diferencia | {dif:.2f}|\n"\
            f"| Pixeles en 0 | {zer:.2f}%| Pixeles en 255 | {sat:.2f}%|\n\n "
    return texto

def Escribir_MD(texto, path = "Readme.md"):
    
    with open(path, "a+") as file:
        file.seek(0)
        if len(file.read(1)) == 0:
            cabecera = "# Archivo de Resultados\n"\
                       "Analisis realizados utilizando el script {:}, cuyo `Help` el siguiente:\n\n```python\n{:}```\n\n".format(__file__, __doc__)
            file.write(cabecera+texto)

            os.system("chmod a+rw " + path)
        else:
            file.write(texto)

# In[2]: Variables globales
colores = ["r","g","b","m","c","y"]
lineas  = ['-', '--', '-.', ':']
estilos = [(c, l) for l in lineas for c in colores]
estilo = 0

#Editar esta linea segun cada caso
Espesores = [0.1,0.4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]

# In[3]: Procesamiento
with fits.open(args.c + "/Captura.fits") as hduc, fits.open(args.f + "/Captura.fits") as hdfs, fits.open(args.l + "/Captura.fits") as hdff:

    #Defino imagen
    captura = hduc[0].data
    fondos  = hdfs[0].data
    flat    = hdff[0].data
        
    imagen  = (captura - fondos)/(flat - fondos)

    #Media del flat
    mf = flat.reshape(-1).mean()

    #Imagen de diferencias
    name=args.c + "/Diferencia.fits"
    hdu=fits.PrimaryHDU()
    hdu.data=imagen
    hdu.writeto(name,overwrite=True)

    #Figura con cuadrados
    fig, ax = plt.subplots()
    ax.imshow(imagen, cmap='gray')

    name = "Rectangulos.pickle"
    with open(name, 'rb') as file:
        areas = pickle.load(file)

        texto = "## Nuevo Analisis\n"\
            f"* Captura: {args.c}\n"\
            f"* Fondo: {args.f}\n"\
            f"* Flat: {args.l}\n"\
            f"![]({args.c}/Diferencia.png)\n\n"
        
        Escribir_MD(texto)

        for i,area in enumerate(areas):
            data = imagen[area[2]:area[3],area[0]:area[1]]

            zer = 100*np.count_nonzero(data == 0)/data.size
            sat = 100*np.count_nonzero(data == 255)/data.size

            imgA, imgB = Separar(data)
            imgA = imgA.reshape(-1)
            imgB = imgB.reshape(-1)

            medA = imgA.mean()
            medB = imgB.mean()
            stdA = imgA.std()
            stdB = imgB.std()
            med  = (medA + medB)*0.5
            dif  = medB - medA
            mux  = - np.log(med/mf)

            texto = "| Resultados ||||\n|-|-|-|-|\n"\
            f"| Media A | {medA:.2f}| Media B | {medB:.2f}|\n"\
            f"| Std A | {stdA:.2f}| Std B | {stdB:.2f}|\n"\
            f"| Media | {med:.2f}| Diferencia | {dif:.2f}|\n"\
            f"| Espesor | {Espesores[i]:.2f}| mux | {mux:.2f}|\n"\
            f"| Pixeles en 0 | {zer:.2f}%| Pixeles en 255 | {sat:.2f}%|\n"\
            f"| Rectangulo | {estilos[estilo][0]}{estilos[estilo][1]}| Valores | {area}|\n\n"

            Escribir_MD(texto)

            rect = patches.Rectangle((area[0], area[2]), (area[1]-area[0]), (area[3]-area[2]), linewidth=1, edgecolor=estilos[estilo][0], linestyle=estilos[estilo][1], facecolor='none')
            ax.add_patch(rect)
            
            estilo = estilo + 1
    
    fig.savefig(args.c + "/Diferencia")
    #plt.show()