# Archivo de Resultados
Analisis realizados utilizando el script Analizar.py, cuyo `Help` el siguiente:

```python

Definir.py

El script permite definir regiones rectangulares donde se realizar posteriores análisis,
almacenando un archivo que contiene sus definiciones.

Utilización:
-f, --fondo:       Carpeta de fondo
-c, --captura:     Carpeta de capturas

Requerimientos:
Para correr este script se requiere que los siguientes módulos estén instalados:
- os
- numpy
- astropy.io
- argparse
- sys
- matplotlib

@Author: Leo Corzi
@Email: damian.corzi@ib.edu.ar
@Date: Thu May 26 2022, 12:48:41
```

## Nuevo Analisis
* Captura: prueba4_escalera_30kV_40uA_exp-16383.gan-32.off-2
* Fondo: fondo_sin_radiacion_exp-16383.gan-32.off-2
* Flat: fondo_con_radiacion_30kV_40uA_exp-16383.gan-32.off-2
![](prueba4_escalera_30kV_40uA_exp-16383.gan-32.off-2/Diferencia.png)

| Resultados ||||
|-|-|-|-|
| Media A | 0.51| Media B | 0.51|
| Std A | 0.09| Std B | 0.09|
| Media | 0.51| Diferencia | -0.00|
| Espesor | 0.10| mux | 5.38|
| Pixeles en 0 | 0.00%| Pixeles en 255 | 0.00%|
| Rectangulo | r-| Valores | [31, 220, 41, 973]|

| Resultados ||||
|-|-|-|-|
| Media A | 0.15| Media B | 0.15|
| Std A | 0.04| Std B | 0.04|
| Media | 0.15| Diferencia | -0.00|
| Espesor | 0.40| mux | 6.63|
| Pixeles en 0 | 0.00%| Pixeles en 255 | 0.00%|
| Rectangulo | g-| Valores | [358, 999, 48, 972]|

| Resultados ||||
|-|-|-|-|
| Media A | 1.04| Media B | 1.04|
| Std A | 0.14| Std B | 0.14|
| Media | 1.04| Diferencia | 0.00|
| Espesor | 1.00| mux | 4.66|
| Pixeles en 0 | 0.00%| Pixeles en 255 | 0.00%|
| Rectangulo | b-| Valores | [1107, 1235, 52, 972]|

