"""
Definir.py

El script permite definir regiones rectangulares donde se realizar posteriores análisis,
almacenando un archivo que contiene sus definiciones.

Utilización:
-f, --fondo:       Carpeta de fondo
-c, --captura:     Carpeta de capturas
-d, --diferencia:  Exporta imagen sin fondo

Requerimientos:
Para correr este script se requiere que los siguientes módulos estén instalados:
- os
- numpy
- astropy.io
- argparse
- sys
- matplotlib

@Author: Leo Corzi
@Email: damian.corzi@ib.edu.ar
@Date: Thu May 26 2022, 12:48:41
"""

import os
import numpy as np
from astropy.io import fits
import argparse
from scipy.stats import norm
import matplotlib.patches as patches
import matplotlib.pyplot as plt
from matplotlib.widgets import Cursor, Button
import pickle

# In[0]: Parser 
parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)

#Lista de argumentos esperados, tipo y valores por defecto
parser.add_argument('-f', '--fondo', type=str, action="store", dest='f', default="fondo_con_radiacion_30kV_160uA_exp-16383.gan-32.off-2",     help='Carpeta de fondo')
parser.add_argument('-c', '--captura', type=str, action="store", dest='c', default="prueba4_escalera_30kV_40uA_exp-16383.gan-32.off-2", help='Carpeta de captura')
parser.add_argument('-d', '--diferencia',        action="store_true", dest='d',               help='Exporta imagen sin fondo')

#Variable que almacena los resultados
args = parser.parse_args()

# In[1]: Funciones Adicionales

def Directorio(path):
	if os.path.exists(path):
		os.system("rm " + path + "/*")
	else:
 		os.makedirs(path)

def Eliminar(path):
	if os.path.exists(path):
		os.system("rm " + path)

def Comando(orden):
	os.system(orden)

def TiempoEjec(t_eje):
	if t_eje > 3600:
		return str(t_eje/3600) + " hs"
	elif t_eje > 60:
		return str(t_eje/60) + " min"
	else :
		return str(t_eje) + " seg"

def Almacenar(NameFile,Parametros):
	Eliminar(NameFile)
	archivo = open(NameFile, 'wb')
	pickle.dump(Parametros, archivo)
	archivo.close()

# In[2]: Variables globales

x0 = 0
y0 = 0
areas = []
base_scale = 2.0

#Figura a graficar
fig, axd = plt.subplot_mosaic(
            [['fig', 'his']],
            gridspec_kw=dict(width_ratios=[1, 1], height_ratios=[1]),
            figsize=(20, 10), constrained_layout=True
        )

#Funciones que requieren de las variables globales
def anterior(event):
	if len(axd["fig"].patches) > 0:
		axd["fig"].patches.pop()
		fig.canvas.draw()
		areas.pop()

def guardar(event):
	name = "Rectangulos.pickle"
	Eliminar(name)
	archivo = open(name, 'wb')
	pickle.dump(areas, archivo)
	archivo.close()
	print("Rectangulos:\n",areas,"\n")
	
def Separar(img):
	rows, cols = np.indices(img.shape)
	imgA = img[(rows%2)==(cols%2)]
	imgB = img[(rows%2)!=(cols%2)]

	return imgA, imgB

def click_in(event):
	if event.inaxes is axd["fig"]:
		global x0, y0
		x0 = event.xdata
		y0 = event.ydata
	
def click_out(event):
	if event.inaxes is axd["fig"]:
		x1 = event.xdata
		y1 = event.ydata

		x0p, x1p = int(min([x0, x1])), int(max([x0, x1]))
		y0p, y1p = int(min([y0, y1])), int(max([y0, y1]))
	
		if (x1p - x0p) < 4 or (y1p - y0p) < 4:
			return False

		data = imagen[y0p:y1p,x0p:x1p]
		
		if data.size == 0:
			return False

		rect = patches.Rectangle((x0, y0), (x1-x0), (y1-y0), linewidth=1, edgecolor='r', facecolor='none')
		axd["fig"].add_patch(rect)

		imgA, imgB = Separar(data)

		imgA = imgA.reshape(-1)
		imgB = imgB.reshape(-1)

		axd["his"].cla()

		dataA=axd["his"].hist(imgA, bins=256, density=True, alpha=0.6, color='g')
		xminA, xmaxA = min(dataA[1]), max(dataA[1])
		xA = np.linspace(xminA, xmaxA, 100)
		muA, stdA = norm.fit(imgA)
		pA = norm.pdf(xA, muA, stdA)
		axd["his"].plot(xA, pA, 'k', linewidth=2)

		dataB=axd["his"].hist(imgB, bins=256, density=True, alpha=0.6, color='r')
		xminB, xmaxB = min(dataB[1]), max(dataB[1])
		xB = np.linspace(xminB, xmaxB, 100)
		muB, stdB = norm.fit(imgB)
		pB = norm.pdf(xB, muB, stdB)
		axd["his"].plot(xB, pB, 'k', linewidth=2)

		fig.canvas.draw()

		areas.append([x0p,x1p,y0p,y1p])

def zoom(event):
	if event.inaxes is axd["fig"]:
    	
		cur_xlim = axd["fig"].get_xlim()
		cur_ylim = axd["fig"].get_ylim()

		xdata = event.xdata
		ydata = event.ydata

		if event.button == 'up':
			scale_factor = 1 / base_scale
		elif event.button == 'down':
			scale_factor = base_scale
		else:
			scale_factor = 1

		new_width = (cur_xlim[1] - cur_xlim[0]) * scale_factor
		new_height = (cur_ylim[1] - cur_ylim[0]) * scale_factor

		relx = (cur_xlim[1] - xdata)/(cur_xlim[1] - cur_xlim[0])
		rely = (cur_ylim[1] - ydata)/(cur_ylim[1] - cur_ylim[0])

		axd["fig"].set_xlim([xdata - new_width * (1-relx), xdata + new_width * (relx)])
		axd["fig"].set_ylim([ydata - new_height * (1-rely), ydata + new_height * (rely)])
		axd["fig"].figure.canvas.draw()

def on_key(event):
    cur_xlim = axd["fig"].get_xlim()
    cur_ylim = axd["fig"].get_ylim()

    dx = (cur_xlim[1] - cur_xlim[0]) * 0.1
    dy = (cur_ylim[1] - cur_ylim[0]) * 0.1

    if event.key == "left":
        cur_xlim += dx
    elif event.key == "right":
        cur_xlim -= dx
    elif event.key == "up":
        cur_ylim -= dy
    elif event.key == "down":
        cur_ylim += dy

    axd["fig"].set_xlim(cur_xlim)
    axd["fig"].set_ylim(cur_ylim)
    axd["fig"].figure.canvas.draw()

#Interactividad
fig.canvas.mpl_connect('button_press_event', click_in)
fig.canvas.mpl_connect('button_release_event', click_out)
fig.canvas.mpl_connect('scroll_event', zoom)
fig.canvas.mpl_connect('key_press_event', on_key)
Cursor(axd["fig"], horizOn=True, vertOn=True, useblit=True, color='blue', linewidth=1)

axprev = plt.axes([0.1, 0.01, 0.1, 0.075])
bprev = Button(axprev, 'anterior')
bprev.on_clicked(anterior)

axsave = plt.axes([0.3, 0.01, 0.1, 0.075])
bsave = Button(axsave, 'guardar')
bsave.on_clicked(guardar)

# In[3]: Procesamiento
with fits.open(args.f + "/Captura.fits") as hduf, fits.open(args.c + "/Captura.fits") as hduc:

	fondo = hduf[0].data
	captura = hduc[0].data
	imagen =  fondo - captura

	if args.d:
		name="Diferencia.fits"
		hdu=fits.PrimaryHDU()
		hdu.data=imagen
		hdu.writeto(name,overwrite=True)
	
	axd["fig"].imshow(imagen, cmap='gray', vmin=0, vmax=255)
	plt.show()

exit()

#Version con clase, no funciona por los botones, buscar otro metodo!

class Definiciones:
	def __init__(self, imagen):

		self.imagen = imagen
		
		#Parametros Auxiliares
		self.x0 = 0
		self.y0 = 0
		self.areas = []

		#Graficas
		self.fig = None
		self.axd = None

		#Interactividad
		self.cin  = None
		self.cout = None
		self.cursor = None
	
		return None
	
	def Graficar(self):
		self.fig, self.axd = plt.subplot_mosaic(
            [['fig', 'his']],
            gridspec_kw=dict(width_ratios=[1, 1], height_ratios=[1]),
            figsize=(10, 5), constrained_layout=True
        )

		self.axd["fig"].imshow(imagen, cmap='gray', vmin=0, vmax=255)

	def Interactivar(self):
		self.cin  = self.fig.canvas.mpl_connect('button_press_event', self.click_in)
		self.cout = self.fig.canvas.mpl_connect('button_release_event', self.click_out)
		self.cursor = Cursor(self.axd["fig"], horizOn=True, vertOn=True, useblit=True, color='blue', linewidth=1)
		
		axnext = plt.axes([0.81, 0.05, 0.1, 0.075])
		bnext = Button(axnext, 'anterior')
		bnext.on_clicked(self.anterior)

		axprev = plt.axes([0.7, 0.05, 0.1, 0.075])
		bprev = Button(axprev, 'guardar')
		bprev.on_clicked(self.guardar)

	def click_in(self,event):
		if event.inaxes is self.axd["fig"]:
			self.x0 = event.xdata
			self.y0 = event.ydata
	
	def click_out(self,event):
		if event.inaxes is self.axd["fig"]:
			x1 = event.xdata
			y1 = event.ydata

			x0p, x1p = int(min([self.x0, x1])), int(max([self.x0, x1]))
			y0p, y1p = int(min([self.y0, y1])), int(max([self.y0, y1]))
	
			if x0p == x1p or y0p == y1p:
				return False

			rect = patches.Rectangle((self.x0, self.y0), (x1-self.x0), (y1-self.y0), linewidth=1, edgecolor='r', facecolor='none')
			self.axd["fig"].add_patch(rect)

			data = self.imagen[x0p:x1p,y0p:y1p]

			imgA, imgB = self.Separar(data)

			imgA = imgA.reshape(-1)
			imgB = imgB.reshape(-1)

			self.axd["his"].cla()

			dataA=self.axd["his"].hist(imgA, bins=256, density=True, alpha=0.6, color='g')
			xminA, xmaxA = min(dataA[1]), max(dataA[1])
			xA = np.linspace(xminA, xmaxA, 100)
			muA, stdA = norm.fit(imgA)
			pA = norm.pdf(xA, muA, stdA)
			self.axd["his"].plot(xA, pA, 'k', linewidth=2)

			dataB=self.axd["his"].hist(imgB, bins=256, density=True, alpha=0.6, color='r')
			xminB, xmaxB = min(dataB[1]), max(dataB[1])
			xB = np.linspace(xminB, xmaxB, 100)
			muB, stdB = norm.fit(imgB)
			pB = norm.pdf(xB, muB, stdB)
			self.axd["his"].plot(xB, pB, 'k', linewidth=2)

			self.fig.canvas.draw()

			self.areas.append([x0p,x1p,y0p,y1p])
			print(self.areas)
	
	def anterior(self):
		if len(self.axd["fig"].patches) > 0:
			self.axd["fig"].patches.pop()
			self.fig.canvas.draw()
			self.areas.pop()
			print(self.areas)

	def guardar(self,):
		Almacenar("hola.pickle",self.areas)
		print("almacenar")
	
	def Separar(self,img):
		rows, cols = np.indices(img.shape)
		imgA = img[(rows%2)==(cols%2)]
		imgB = img[(rows%2)!=(cols%2)]

		return imgA, imgB

with fits.open(args.f + "/Captura.fits") as hduf, fits.open(args.c + "/Captura.fits") as hduc:

	fondo = hduf[0].data
	captura = hduc[0].data

	imagen = captura - fondo

	name="Diferencia.fits"
	hdu=fits.PrimaryHDU()
	hdu.data=imagen
	hdu.writeto(name,overwrite=True)

	hola = Definiciones(imagen)
	hola.Graficar()
	hola.Interactivar()
	plt.show()
