"""
https://bitbucket.org/spekpy/spekpy_release/wiki/Home
"""

import spekpy as sp # Import SpekPy
import matplotlib.pyplot as plt # Import library for plotting
import numpy as np
from scipy.interpolate import interp1d

#Formula de acetato de celulosa, simil madera
C = 12 * 20
H = 1  * 28
O = 16 * 12
CHO = C + H + O

#Espesor de la madera
x = 2

#Variables intermedias, espesores aproximados de componentes de la madera
xc = x * C/CHO
xh = x * H/CHO
xo = x * O/CHO

#Coeficientes de "absorcion" masica del Silicio (MU/roh_en) NIST
SI = np.array([
[   1.00000E-03,  1.570E+03,  1.567E+03 ],
[   1.50000E-03,  5.355E+02,  5.331E+02 ],
[   1.83890E-03,  3.092E+02,  3.070E+02 ],
[   1.83890E-03,  3.192E+03,  3.059E+03 ],
[   2.00000E-03,  2.777E+03,  2.669E+03 ],
[   3.00000E-03,  9.784E+02,  9.516E+02 ],
[   4.00000E-03,  4.529E+02,  4.427E+02 ],
[   5.00000E-03,  2.450E+02,  2.400E+02 ],
[   6.00000E-03,  1.470E+02,  1.439E+02 ],
[   8.00000E-03,  6.468E+01,  6.313E+01 ],
[   1.00000E-02,  3.389E+01,  3.289E+01 ],
[   1.50000E-02,  1.034E+01,  9.794E+00 ],
[   2.00000E-02,  4.464E+00,  4.076E+00 ],
[   3.00000E-02,  1.436E+00,  1.164E+00 ],
[   4.00000E-02,  7.012E-01,  4.782E-01 ],
[   5.00000E-02,  4.385E-01,  2.430E-01 ],
[   6.00000E-02,  3.207E-01,  1.434E-01 ],
[   8.00000E-02,  2.228E-01,  6.896E-02 ],
[   1.00000E-01,  1.835E-01,  4.513E-02 ],
[   1.50000E-01,  1.448E-01,  3.086E-02 ],
[   2.00000E-01,  1.275E-01,  2.905E-02 ],
[   3.00000E-01,  1.082E-01,  2.932E-02 ],
[   4.00000E-01,  9.614E-02,  2.968E-02 ],
[   5.00000E-01,  8.748E-02,  2.971E-02 ],
[   6.00000E-01,  8.077E-02,  2.951E-02 ],
[   8.00000E-01,  7.082E-02,  2.875E-02 ],
[   1.00000E+00,  6.361E-02,  2.778E-02 ],
[   1.25000E+00,  5.688E-02,  2.652E-02 ],
[   1.50000E+00,  5.183E-02,  2.535E-02 ],
[   2.00000E+00,  4.480E-02,  2.345E-02 ],
[   3.00000E+00,  3.678E-02,  2.101E-02 ],
[   4.00000E+00,  3.240E-02,  1.963E-02 ],
[   5.00000E+00,  2.967E-02,  1.878E-02 ],
[   6.00000E+00,  2.788E-02,  1.827E-02 ],
[   8.00000E+00,  2.574E-02,  1.773E-02 ],
[   1.00000E+01,  2.462E-02,  1.753E-02 ],
[   1.50000E+01,  2.352E-02,  1.746E-02 ],
[   2.00000E+01,  2.338E-02,  1.757E-02 ]
])

#Parametros para la interpolacion de la curva en nuevos puntos de interes
X = SI.transpose()[0]*1000
Y = SI.transpose()[2]
C = interp1d(X, Y)

# In[0]: Definidcion de funciones utiles

#Espectro en la superficie del sensor considerando un filtro de aluminio,
#El espesor de la muestra y la atenuacion en la lamina de aluminio
def Espectro(kv, x):

    #Generacion de espectro
    #th = angulo del tubo
    s = sp.Spek(kvp=kv,th=12)

    #Filtrado
    s.filter('Al',x)

    #Atenuacion en muestra
    s.filter('C',xc)
    s.filter('H',xh)
    s.filter('O',xo)

    #Atenuacion en Lamina protectora
    s.filter('Al',0.1)

    #Espectro optenido del filtrado
    k, f = s.get_spectrum(edges=True)

    return k, f

#Obtencion de puntos a partir del fiteo de la curva
def NuevosPuntos(CurvaFit, X, E):

    #Energias donde calcular
    En = E[E<X[-1]]

    #MU en los puntos requeridos
    Mu  = CurvaFit(En)

    return En, Mu

#Eficiencia de la conversion 
def Eficiencia(CurvaFit, X, E, F0, x=10E-3):

    #Energias donde calcular
    En = E[E<X[-1]]

    #MU en los puntos requeridos
    Mu  = CurvaFit(En)

    #Fluencias 
    Ef = F0*(1-np.exp(-Mu*x))

    return En, Ef

#Valor medio de la energia del espectro obtenido
def EnergiaMedia(X,Y):
    return sum(X*Y)/sum(Y)

#Encontrar un espesor de filtro x que genere un
#valor medio de energia M en un espectro generado
#por un kilovoltaje K
def Busqueda(kv,M):

    for x in np.arange(0,30,0.1):
        k, f = Espectro(kv,x)
        m = sum(k*f)/sum(f)
        if m > M:
            return x

#Encontrar un espesor de filtro x que genere un
#valor medio de energia M en un espectro generado
#por un kilovoltaje K teniendo en cuenta la eficiencia del Si
def Busqueda2(E,M,C,X):

    for x in np.arange(4,30,0.01):
        k, f = Espectro(E,x)
        k, f = Eficiencia(C, X, k, f)
        m = sum(k*f)/sum(f)
        if m > M:
            return x, sum(f)

#En, Mu = Interpolacion(C,X,SI.transpose()[0]*1000)

#plt.plot(En, Mu)
#plt.xscale("log")
#plt.yscale("log")
#plt.show()
#exit()

#print(Busqueda(80,40))
#exit()

#print(Busqueda2(80,40,C,X))
#exit()

plt.figure(0)
kv, es = 40, 1
k, f = Espectro(kv, es)
txt = f"{kv}kV, {es}mm, {EnergiaMedia(k,f):.1f}kV"
plt.plot(k, f, "r", label=txt)

kv, es = 60, 1
k, f = Espectro(kv, es)
txt = f"{kv}kV, {es}mm, {EnergiaMedia(k,f):.1f}kV"
plt.plot(k, f, "g", label=txt)

kv, es = 80, 1
k, f = Espectro(kv, es)
txt = f"{kv}kV, {es}mm, {EnergiaMedia(k,f):.1f}kV"
plt.plot(k, f, "b", label=txt)

plt.xlabel('Energy [keV]')
plt.ylabel('Fluence per mAs per unit energy [photons/cm2/mAs/keV]')
plt.title('An example x-ray spectrum')
plt.legend()


plt.figure(1)
kv, es = 40, 1
k, f = Espectro(kv, es)
k, f = Eficiencia(C, X, k, f, 10**-3)
txt = f"{kv}kV, {es}mm, {EnergiaMedia(k,f):.1f}kV"
plt.plot(k, f, "r", label=txt)

kv, es = 60, 1
k, f = Espectro(kv, es)
k, f = Eficiencia(C, X, k, f, 10**-3)
txt = f"{kv}kV, {es}mm, {EnergiaMedia(k,f):.1f}kV"
plt.plot(k, f, "g", label=txt)

kv, es = 80, 1
k, f = Espectro(kv, es)
k, f = Eficiencia(C, X, k, f, 10**-3)
txt = f"{kv}kV, {es}mm, {EnergiaMedia(k,f):.1f}kV"
plt.plot(k, f, "b", label=txt)

plt.xlabel('Energy [keV]')
plt.ylabel('Fluence per mAs per unit energy [photons/cm2/mAs/keV]')
plt.title('An example x-ray spectrum')
plt.legend()
plt.show()
exit()

plt.figure(1)
kv, es = 40, 1
k, f = Espectro(kv, es)
txt = f"{kv}kV, {es}mm, {EnergiaMedia(k,f):.1f}kV"
plt.plot(k, f, "r", label=txt)

kv, es = 40, 2
k, f = Espectro(kv, es)
txt = f"{kv}kV, {es}mm, {EnergiaMedia(k,f):.1f}kV"
plt.plot(k, f, "g", label=txt)

kv, es = 40, 4
k, f = Espectro(kv, es)
txt = f"{kv}kV, {es}mm, {EnergiaMedia(k,f):.1f}kV"
plt.plot(k, f, "b", label=txt)

plt.xlabel('Energy [keV]')
plt.ylabel('Fluence per mAs per unit energy [photons/cm2/mAs/keV]')
plt.title('An example x-ray spectrum')
plt.legend()


plt.figure(2)
kv, es = 40, 1
k, f = Espectro(kv, es)
txt = f"{kv}kV, {es}mm, {EnergiaMedia(k,f):.1f}kV"
plt.plot(k, f, "r", label=txt)

kv, es = 60, 2
k, f = Espectro(kv, es)
txt = f"{kv}kV, {es}mm, {EnergiaMedia(k,f):.1f}kV"
plt.plot(k, f, "g", label=txt)

kv, es = 80, 4
k, f = Espectro(kv, es)
txt = f"{kv}kV, {es}mm, {EnergiaMedia(k,f):.1f}kV"
plt.plot(k, f, "b", label=txt)

plt.xlabel('Energy [keV]')
plt.ylabel('Fluence per mAs per unit energy [photons/cm2/mAs/keV]')
plt.title('An example x-ray spectrum')
plt.legend()


plt.figure(3)
kv, es = 40, 0.77
k, f = Espectro(kv, es)
txt = f"{kv}kV, {es}mm, {EnergiaMedia(k,f):.1f}kV"
plt.plot(k, f, "r", label=txt)

kv, es = 60, 3.21
k, f = Espectro(kv, es)
txt = f"{kv}kV, {es}mm, {EnergiaMedia(k,f):.1f}kV"
plt.plot(k, f, "g", label=txt)

kv, es = 80, 7.9
k, f = Espectro(kv, es)
txt = f"{kv}kV, {es}mm, {EnergiaMedia(k,f):.1f}kV"
plt.plot(k, f, "b", label=txt)

plt.xlabel('Energy [keV]')
plt.ylabel('Fluence per mAs per unit energy [photons/cm2/mAs/keV]')
plt.title('An example x-ray spectrum')
plt.legend()

plt.figure(4)
ax = np.arange(10,100,1)
ax , ay = NuevosPuntos(C, X, ax)
aa = 1-np.exp(-ay*10E-3)
plt.plot(ax, aa)

plt.figure(5)
kv, es = 40, 0.77
k, f = Espectro(kv, es)
k, f = Eficiencia(C, X, k, f)
r = sum(f)
txt = f"{kv}kV, {es}mm, {EnergiaMedia(k,f):.1f}kV, {r/sum(f):.2f}"
plt.plot(k, f, "r", label=txt)

kv, es = 60, 3.21
k, f = Espectro(kv, es)
k, f = Eficiencia(C, X, k, f)
txt = f"{kv}kV, {es}mm, {EnergiaMedia(k,f):.1f}kV, {r/sum(f):.2f}"
f = f * r/sum(f)
plt.plot(k, f, "g", label=txt)

kv, es = 80, 7.9
k, f = Espectro(kv, es)
k, f = Eficiencia(C, X, k, f)
txt = f"{kv}kV, {es}mm, {EnergiaMedia(k,f):.1f}kV, {r/sum(f):.2f}"
f = f * r/sum(f)
plt.plot(k, f, "b", label=txt)

plt.xlabel('Energy [keV]')
plt.ylabel('Fluence per mAs per unit energy [photons/cm2/mAs/keV]')
plt.title('An example x-ray spectrum')
plt.legend()
plt.show()