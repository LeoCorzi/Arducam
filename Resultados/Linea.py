"""
Definir.py

El script permite definir regiones rectangulares donde se realizar posteriores análisis,
almacenando un archivo que contiene sus definiciones.

Utilización:
-f, --fondo:       Carpeta de fondo
-c, --captura:     Carpeta de capturas
-d, --diferencia:  Exporta imagen sin fondo

Requerimientos:
Para correr este script se requiere que los siguientes módulos estén instalados:
- os
- numpy
- astropy.io
- argparse
- sys
- matplotlib

@Author: Leo Corzi
@Email: damian.corzi@ib.edu.ar
@Date: Thu May 26 2022, 12:48:41
"""

import os
import numpy as np
from astropy.io import fits
import argparse
from scipy.stats import norm
import matplotlib.patches as patches
import matplotlib.pyplot as plt
from matplotlib.widgets import Cursor, Button
import pickle

# In[0]: Parser 
parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)

#Lista de argumentos esperados, tipo y valores por defecto
parser.add_argument('-f', '--fondo', type=str, action="store", dest='f', default="fondo_sin_radiacion_exp-16383.gan-9.off-2",     help='Carpeta de fondo')
parser.add_argument('-c', '--captura', type=str, action="store", dest='c', default="Madera_AE61_20kV_160uA_exp-16383.gan-9.off-2", help='Carpeta de captura')
parser.add_argument('-l', '--flat',    type=str, action="store", dest='l', default="fondo_con_radiacion_20kV_160uA_exp-16383.gan-9.off-2", help='Carpeta de flat')
parser.add_argument('-a', '--ancho', type=int, action="store", dest='a', default=10, help='Ancho de eveluacion')
parser.add_argument('-e', '--espesor', type=int, action="store", dest='e', default=0.01, help='Espesor en centimetros')

#Variable que almacena los resultados
args = parser.parse_args()

# In[1]: Funciones Adicionales

def Directorio(path):
	if os.path.exists(path):
		os.system("rm " + path + "/*")
	else:
 		os.makedirs(path)

def Eliminar(path):
	if os.path.exists(path):
		os.system("rm " + path)

def Comando(orden):
	os.system(orden)

def Almacenar(NameFile,Parametros):
	Eliminar(NameFile)
	archivo = open(NameFile, 'wb')
	pickle.dump(Parametros, archivo)
	archivo.close()

# In[2]: Variables globales
x0 = 0
y0 = 0

#Figura a graficar
fig, axd = plt.subplot_mosaic(
            [['fig', 'br'],
			 ['fig', 'mu']],
            gridspec_kw=dict(width_ratios=[1, 1], height_ratios=[1,1]),
            figsize=(10, 5), constrained_layout=True
        )

axd["br"].set_xlabel("Pixeles")
axd["br"].set_ylabel("Transmisión")
axd["br"].grid(True)

axd["mu"].set_xlabel("Pixeles")
axd["mu"].set_ylabel("muX")
axd["mu"].grid(True)

#Funciones que requieren de las variables globales
def anterior(event):
	if len(axd["fig"].patches) > 0:
		axd["fig"].patches.pop()
		fig.canvas.draw()

def guardar(event):
	print("No configurado\n")
	
def Separar(img):
	rows, cols = np.indices(img.shape)
	imgA = img[(rows%2)==(cols%2)]
	imgB = img[(rows%2)!=(cols%2)]

	return imgA, imgB

def click_in(event):
	if event.inaxes is axd["fig"]:
		global x0, y0
		x0 = event.xdata
		y0 = event.ydata
	
def click_out(event):
	if event.inaxes is axd["fig"]:
		x1 = event.xdata

		x0p, x1p = int(min([x0, x1])), int(max([x0, x1]))
		y0p      = int(y0)
	
		if (x1p - x0p) < 4:
			return False

		X = np.arange(x0p,x1p)
		Y = np.arange((X.size),dtype=np.float32)

		for i,x in enumerate(X):
			Y[i] = imagen[y0p-args.a:y0p+args.a,x-args.a:x+args.a].reshape(-1).mean()

		color = "w"
		con = patches.ConnectionPatch((x0p,y0p), (x1p, y0p), "data", "data",
                      arrowstyle="-|>", shrinkA=5, shrinkB=5,
                      mutation_scale=20, fc=color, ec=color)
		axd["fig"].add_patch(con)

		#Valores
		mu = - np.log(Y)/args.e

		axd["br"].cla()
		axd["br"].plot(X, Y, 'g')

		axd["mu"].cla()
		axd["mu"].plot(X, mu, 'g')

		axd["br"].set_xlabel("Pixeles")
		axd["br"].set_ylabel("Transmisión")
		axd["br"].grid(True)

		axd["mu"].set_xlabel("Pixeles")
		axd["mu"].set_ylabel("muX")
		axd["mu"].grid(True)

		fig.canvas.draw()

def zoom(event):
	if event.inaxes is axd["fig"]:
		base_scale = 2.0

		cur_xlim = axd["fig"].get_xlim()
		cur_ylim = axd["fig"].get_ylim()

		xdata = event.xdata
		ydata = event.ydata

		if event.button == 'up':
			scale_factor = 1 / base_scale
		elif event.button == 'down':
			scale_factor = base_scale
		else:
			scale_factor = 1

		new_width = (cur_xlim[1] - cur_xlim[0]) * scale_factor
		new_height = (cur_ylim[1] - cur_ylim[0]) * scale_factor

		relx = (cur_xlim[1] - xdata)/(cur_xlim[1] - cur_xlim[0])
		rely = (cur_ylim[1] - ydata)/(cur_ylim[1] - cur_ylim[0])

		axd["fig"].set_xlim([xdata - new_width * (1-relx), xdata + new_width * (relx)])
		axd["fig"].set_ylim([ydata - new_height * (1-rely), ydata + new_height * (rely)])
		axd["fig"].figure.canvas.draw()

def on_key(event):
    cur_xlim = axd["fig"].get_xlim()
    cur_ylim = axd["fig"].get_ylim()

    dx = (cur_xlim[1] - cur_xlim[0]) * 0.1
    dy = (cur_ylim[1] - cur_ylim[0]) * 0.1

    if event.key == "left":
        cur_xlim += dx
    elif event.key == "right":
        cur_xlim -= dx
    elif event.key == "up":
        cur_ylim -= dy
    elif event.key == "down":
        cur_ylim += dy

    axd["fig"].set_xlim(cur_xlim)
    axd["fig"].set_ylim(cur_ylim)
    axd["fig"].figure.canvas.draw()

#Interactividad
fig.canvas.mpl_connect('button_press_event', click_in)
fig.canvas.mpl_connect('button_release_event', click_out)
fig.canvas.mpl_connect('scroll_event', zoom)
fig.canvas.mpl_connect('key_press_event', on_key)
Cursor(axd["fig"], horizOn=True, vertOn=True, useblit=True, color='blue', linewidth=1)

axprev = plt.axes([0.1, 0.01, 0.1, 0.075])
bprev = Button(axprev, 'anterior')
bprev.on_clicked(anterior)

axsave = plt.axes([0.3, 0.01, 0.1, 0.075])
bsave = Button(axsave, 'guardar')
bsave.on_clicked(guardar)

# In[3]: Procesamiento
with fits.open(args.c + "/Captura.fits") as hduc, fits.open(args.f + "/Captura.fits") as hdfs, fits.open(args.l + "/Captura.fits") as hdff:

    #Defino imagen
    captura = hduc[0].data
    fondos  = hdfs[0].data
    flat    = hdff[0].data

    imagen  = (captura - fondos)/(flat - fondos)

    axd["fig"].imshow(imagen, cmap='gray')

    plt.show()

exit()