#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Capturar.py

El script permite capturar imagenes y analizar su contenido, generando un archivo
tipo MarkDown con los resultados obtenidos. El mismo debe ser ejecutado como root.

Utilización:
-c, --config:       Archivo de configuracion
-n, --nombre:       Prefijo de capturas
-i, --imagen:       Imagenes a capturar
-e, --exposicion:   Tiempo de exposicion
-g, --ganancia:     Ganancia
-o, --offset:       Offset
-t, --texto:        Texto o comentarios de la medicion

Requerimientos:
Para correr este script se requiere que los siguientes módulos estén instalados:
- arducam_config_parser
- ArducamSDK
- os
- time
- threading
- numpy
- astropy.io
- argparse
- datetime
- sys

Sugerido:
- matplotlib.pyplot

@Author: Leo Corzi
@Email: damian.corzi@ib.edu.ar
@Date: Thu May 26 2022, 12:48:41
"""

import os
import time
import threading
import numpy as np
from astropy.io import fits
import argparse
from datetime import datetime

import sys
sys.path.insert(0, './Librerias')
import arducam_config_parser
import ArducamSDK

try:
    import matplotlib.pyplot as plt
except:
    plt = False

time1 = time.time()
fecha = datetime.now()

# In[0]: Parser 
parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)

#Lista de argumentos esperados, tipo y valores por defecto
parser.add_argument('-c', '--config',     type=str, action="store", dest='c', default="config.cfg", help='Archivo de configuracion')
parser.add_argument('-n', '--nombre',     type=str, action="store", dest='n', default="Imagenes",   help='Prefijo de capturas')
parser.add_argument('-i', '--imagen',     type=int, action="store", dest='i', default=1,            help='Imagenes a capturar')
parser.add_argument('-e', '--exposicion', type=int, action="store", dest='e', default=16383,        help='Tiempo de exposicion')
parser.add_argument('-g', '--ganancia',   type=int, action="store", dest='g', default=9,            help='Ganancia')
parser.add_argument('-o', '--offset',     type=int, action="store", dest='o', default=2,            help='Offset')
parser.add_argument('-t', '--texto',      type=str, action="store", dest='t', default="",           help='Texto o comentarios de la medicion')

#Variable que almacena los resultados
args = parser.parse_args()

# In[1]: Funciones Adicionales

def Vaciar_Directorio(path):
	if os.path.exists(path):
		os.system("rm " + path + "/*")
	else:
 		os.makedirs(path)

def Eliminar(path):
	if os.path.exists(path):
		os.system("rm " + path)

def Comando(orden):
	os.system(orden)

def TiempoEjec(t_eje):
	if t_eje > 3600:
		return str(t_eje/3600) + " hs"
	elif t_eje > 60:
		return str(t_eje/60) + " min"
	else :
		return str(t_eje) + " seg"

def Escribir_MD(medA, medB, med, dif, zer, sat, pathP, path = "Readme.md"):
  
    texto = "## Nueva medicion\n"\
            f"* Nombre: {args.n}\n"\
            f"* Configuracion: {args.c}\n"\
            f"* Cantidad de imagenes: {args.i}\n"\
            f"* Fecha: {str(fecha)[:-7]}\n"\
            f"* Carpeta: {pathP}\n\n"\
            f"Comentarios: {args.t}\n"\
            "| Parametros ||\n|-|-|\n"\
            f"| Exposicion | {args.e}|\n"\
            f"| Ganancia | {args.g}|\n"\
            f"| Offset | {args.o}|\n\n"\
            "| Resultados ||||\n|-|-|-|-|\n"\
            f"| Media A | {medA:.2f}| Media B | {medB:.2f}|\n"\
            f"| Media | {med:.2f}| Diferencia | {dif:.2f}|\n"\
            f"| Pixeles en 0 | {zer:.2f}%| Pixeles en 255 | {sat:.2f}%|\n "\
            "\nHistograma de la imagen capturada:\n"\
            f"![]({pathP}/Histograma.png)\n\n"
    
    with open(path, "a+") as file:
        file.seek(0)
        if len(file.read(1)) == 0:
            cabecera = "# Archivo de Mediciones\n"\
                       "Mediciones realizadas utilizando el script {:}, cuyo `Help` el siguiente:\n\n```python\n{:}```\n\n".format(__file__, __doc__)
            file.write(cabecera+texto)

            os.system("chmod a+rw " + path)
        else:
            file.write(texto)

def Config_File(CNFG_FILE, exposicion, ganancia, offset):	
	file = open(CNFG_FILE,'r')
	temp = file.read()
	file.close()

	temp = temp.replace("PARAM_EXPO","{0:#0{1}x}".format(exposicion,6))
	temp = temp.replace("PARAM_GAIN","{0:#0{1}x}".format(ganancia,6))
	temp = temp.replace("PARAM_OFFS","{0:#0{1}x}".format(offset+1,6))

	name = CNFG_FILE[:-4]+"_usada.cfg"

	Eliminar(name)
	file = open(name,'w')
	file.write(temp)
	file.close()

	os.system("chmod a+rw " + name)

	return name

def Separar(img):
    rows, cols = np.indices(img.shape)
    imgA = img[(rows%2)==(cols%2)]
    imgB = img[(rows%2)!=(cols%2)]

    return imgA, imgB

# In[2]: Funciones Modificadas

#Por eficiencia, dejar esto global
run = True
imagen = None
canalA = None
canalB = None

def configBoard(handle,config):
    ArducamSDK.Py_ArduCam_setboardConfig(handle, config.params[0], \
        config.params[1], config.params[2], config.params[3], \
            config.params[4:config.params_length])

def camera_initFromFile(fileName):

    params = {
        "Width":        0,
        "Height" :      0,
        "save_flag" :   False,
        "color_mode":   0,
        "save_raw":     False,
        "BitWidth":     0,
        "FmtMode":      0
    }

    #Carga el archivo de configuracion
    config = arducam_config_parser.LoadConfigFile(fileName)
    
    camera_parameter = config.camera_param.getdict()

    params["Width"]  = camera_parameter["WIDTH"]
    params["Height"] = camera_parameter["HEIGHT"]
    params["BitWidth"] = camera_parameter["BIT_WIDTH"]

    ByteLength = 1
    if params["BitWidth"] > 8 and params["BitWidth"] <= 16:
        ByteLength = 2
        params["save_raw"] = True
    
    params["FmtMode"] = camera_parameter["FORMAT"][0]
    params["color_mode"] = camera_parameter["FORMAT"][1]

    I2CMode  = camera_parameter["I2C_MODE"]
    I2cAddr  = camera_parameter["I2C_ADDR"]
    TransLvl = camera_parameter["TRANS_LVL"]
    cfg = {"u32CameraType":0x00,
            "u32Width":params["Width"],
            "u32Height":params["Height"],
            "usbType":0,
            "u8PixelBytes":ByteLength,
            "u16Vid":0,
            "u32Size":0,
            "u8PixelBits":params["BitWidth"],
            "u32I2cAddr":I2cAddr,
            "emI2cMode":I2CMode,
            "emImageFmtMode":params["FmtMode"],
            "u32TransLvl":TransLvl }

    ret,handle,rtn_cfg = ArducamSDK.Py_ArduCam_autoopen(cfg)
    if ret == 0:
       
        usb_version = rtn_cfg['usbType']
        configs = config.configs
        configs_length = config.configs_length

        for i in range(configs_length):
            type = configs[i].type

            if ((type >> 16) & 0xFF) != 0 and ((type >> 16) & 0xFF) != usb_version:
                continue
            if type & 0xFFFF == arducam_config_parser.CONFIG_TYPE_REG:
                ArducamSDK.Py_ArduCam_writeSensorReg(handle, configs[i].params[0], configs[i].params[1])
                time.sleep(0.01)
            elif type & 0xFFFF == arducam_config_parser.CONFIG_TYPE_DELAY:
                time.sleep(float(configs[i].params[0])/1000)
            elif type & 0xFFFF == arducam_config_parser.CONFIG_TYPE_VRCMD:
                configBoard(handle,configs[i])
            

        ArducamSDK.Py_ArduCam_registerCtrls(handle, config.controls, config.controls_length)
        return handle, params

    else:
        print("open fail,rtn_val = ",ret)
        return False, False
       
def captureImage_thread(handle):
    global run

    rtn_val = ArducamSDK.Py_ArduCam_beginCaptureImage(handle)
    if rtn_val != 0:
        print("Error beginning capture, rtn_val = ",rtn_val)
        run = False
        return
    
    while run :

        rtn_val = ArducamSDK.Py_ArduCam_captureImage(handle)
        if rtn_val > 255:
            print("Error capture image, rtn_val = ",rtn_val)
            if rtn_val == ArducamSDK.USB_CAMERA_USB_TASK_ERROR:
                break
        
        time.sleep(0.001)
    
    ArducamSDK.Py_ArduCam_endCaptureImage(handle)

def readImage_thread(handle, params, path):
    global run

    #Contador
    saveNum = 0

    #suma
    img_sum = np.zeros((params["Height"],params["Width"]), dtype=np.int32)

    #concat
    con_A = np.zeros(())
    con_B = np.zeros(())

    name_base = path+"/Captura_"

    while (saveNum <= args.i-1) and run:

        if ArducamSDK.Py_ArduCam_availableImage(handle) > 0:		
            rtn_val,data,rtn_cfg = ArducamSDK.Py_ArduCam_readImage(handle)
            datasize = rtn_cfg['u32Size']
            if rtn_val != 0 or datasize == 0:
                ArducamSDK.Py_ArduCam_del(handle)
                print("read data fail!")
                continue

            #Captura de la imagen
            img=np.frombuffer(data,dtype=np.uint8)
            img=np.reshape(img,(params["Height"],params["Width"]))
            
            #Almacenamiento de la imagen
            saveNum += 1
            name=name_base+str(saveNum)+".fits"
            hdu=fits.PrimaryHDU()
            hdu.data=img
            hdu.writeto(name,overwrite=True)
            print(name)

            #Procesamiento de la imagen
            img_sum = img_sum + img.astype(np.int32)    
            imgA, imgB = Separar(img)
            con_A = np.concatenate((con_A, imgA), axis=None)
            con_B = np.concatenate((con_B, imgB), axis=None)

            ArducamSDK.Py_ArduCam_del(handle)

        else:
            time.sleep(0.001)

    run = False

    img_sum = img_sum.astype(np.float32)/saveNum

    hdu=fits.PrimaryHDU()
    hdu.data=img_sum
    hdu.writeto(name_base[:-1] + ".fits",overwrite=True)

    medA = con_A.mean()
    medB = con_B.mean()
    med = (medA + medB)*0.5
    dif = medB - medA
    print("\nPromedio de medias: {:}".format(med))
    print("Diferencia entre medias: {:}".format(dif))

    zer = 100*np.count_nonzero(img_sum == 0)/img_sum.size
    print("Porcentaje pixeles en cero: {:}".format(zer))

    sat = 100*np.count_nonzero(img_sum == 255)/img_sum.size
    print("Porcentaje pixeles saturados: {:}".format(sat))

    Escribir_MD(medA, medB, med, dif, zer, sat, path)

    if plt != False:
        global imagen, canalA, canalB

        imagen = img_sum
        canalA = con_A
        canalB = con_B

# In[3]: Main code

if __name__ == "__main__":

    print("\nExposicion: {:}, Ganancia: {:}, Offset: {:}\n".format(args.e,args.g,args.o))

    path = "Imagenes/" + args.n + "-exp_" + str(args.e) + "-gan_" + str(args.g) + "-off_" + str(args.o) + fecha.strftime("-%m_%d_%Y-%H_%M_%S")

    Vaciar_Directorio(path)

    file = Config_File(args.c,args.e,args.g,args.o)
    handle, params = camera_initFromFile(file)

    if handle != False:

        ArducamSDK.Py_ArduCam_setMode(handle,ArducamSDK.CONTINUOUS_MODE)
        ct = threading.Thread(target=captureImage_thread, args=(handle,))
        rt = threading.Thread(target=readImage_thread,    args=(handle, params, path,))
        ct.start()
        rt.start()

        ct.join()
        rt.join()

        rtn_val = ArducamSDK.Py_ArduCam_close(handle)
        if rtn_val != 0:
            print("device close fail!")

    if plt != False:
        name = "Exposicion: {:}, Ganancia: {:}, Offset: {:}".format(args.e,args.g,args.o)

        fig, axd = plt.subplots(1,2,num=name,figsize=(20,10))
        fig.suptitle(name)
	
        axd[0].imshow(imagen, cmap='gray', vmin=0, vmax=255)
        data=axd[1].hist(canalA, bins=128, density=False, alpha=0.6, color='g', label="canalA")
        data=axd[1].hist(canalB, bins=128, density=False, alpha=0.6, color='r', label="canalB")
        axd[1].legend()
        fig.savefig(path + "/Histograma")
        #plt.show()

    os.system("chmod a+rw " + path)
    os.system("chmod a+rw " + path + "/*.* -R")

    time2 = time.time() - time1
    print("\nTiempo de ejecucion: {:}\n".format(TiempoEjec(time2)))

exit()
