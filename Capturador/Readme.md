# Archivo de Mediciones
Mediciones realizadas utilizando el script Capturar.py, cuyo `Help` el siguiente:

```python

Capturar.py

El script permite capturar imagenes y analizar su contenido, generando un archivo
tipo MarkDown con los resultados obtenidos. El mismo debe ser ejecutado como root.

Utilización:
-c, --config:       Archivo de configuracion
-n, --nombre:       Prefijo de capturas
-i, --imagen:       Imagenes a capturar
-e, --exposicion:   Tiempo de exposicion
-g, --ganancia:     Ganancia
-o, --offset:       Offset
-t, --texto:        Texto o comentarios de la medicion

Requerimientos:
Para correr este script se requiere que los siguientes módulos estén instalados:
- arducam_config_parser
- ArducamSDK
- os
- time
- threading
- numpy
- astropy.io
- argparse
- datetime
- sys

Sugerido:
- matplotlib.pyplot

@Author: Leo Corzi
@Email: damian.corzi@ib.edu.ar
@Date: Thu May 26 2022, 12:48:41
```

## Nueva medicion
* Nombre: Imagenes
* Configuracion: config.cfg
* Cantidad de imagenes: 1
* Fecha: 2022-05-27 12:26:04
* Carpeta: Imagenes/Imagenes-exp_16383-gan_32-off_2-05_27_2022-12_26_04

Comentarios: 
| Parametros ||
|-|-|
| Exposicion | 16383|
| Ganancia | 32|
| Offset | 2|

| Resultados ||||
|-|-|-|-|
| Media A | 233.23| Media B | 233.51|
| Media | 233.37| Diferencia | 0.27|
| Pixeles en 0 | 0.00%| Pixeles en 255 | 82.60%|
 
Histograma de la imagen capturada:
![](Imagenes/Imagenes-exp_16383-gan_32-off_2-05_27_2022-12_26_04/Histograma.png)

## Nueva medicion
* Nombre: Imagenes
* Configuracion: config.cfg
* Cantidad de imagenes: 1
* Fecha: 2022-05-27 12:26:16
* Carpeta: Imagenes/Imagenes-exp_16383-gan_16-off_2-05_27_2022-12_26_16

Comentarios: 
| Parametros ||
|-|-|
| Exposicion | 16383|
| Ganancia | 16|
| Offset | 2|

| Resultados ||||
|-|-|-|-|
| Media A | 240.88| Media B | 241.22|
| Media | 241.05| Diferencia | 0.34|
| Pixeles en 0 | 0.00%| Pixeles en 255 | 80.93%|
 
Histograma de la imagen capturada:
![](Imagenes/Imagenes-exp_16383-gan_16-off_2-05_27_2022-12_26_16/Histograma.png)

## Nueva medicion
* Nombre: Imagenes
* Configuracion: config.cfg
* Cantidad de imagenes: 1
* Fecha: 2022-05-27 12:26:35
* Carpeta: Imagenes/Imagenes-exp_16383-gan_9-off_2-05_27_2022-12_26_35

Comentarios: 
| Parametros ||
|-|-|
| Exposicion | 16383|
| Ganancia | 9|
| Offset | 2|

| Resultados ||||
|-|-|-|-|
| Media A | 139.96| Media B | 141.70|
| Media | 140.83| Diferencia | 1.74|
| Pixeles en 0 | 0.00%| Pixeles en 255 | 0.66%|
 
Histograma de la imagen capturada:
![](Imagenes/Imagenes-exp_16383-gan_9-off_2-05_27_2022-12_26_35/Histograma.png)

